\chapter{Introduction}

\section{Introduction}

Artificial immune systems are a class of bio-inspired algorithms which use the immune system of humans as the basis of their inspiration. They fall in the broader area of nature inspired computation which deals with computational models based on natural phenomenon. Some of the most established branches of nature inspired techniques include diverse models such as evolutionary computation (\ec) based on Darwinian evolution~\cite{back1997handbook}, neural computation based on the working principle of the brain~\cite{fiesler1996handbook}, swarm intelligence inspired by the swarm behaviour of birds~\cite{bonabeau1999swarm}, membrane computing based on the membrane structure of cells~\cite{paun2012membrane} and artificial immune systems. A review of nature inspired techniques can be found in~\cite{de2006fundamentals}. 

Artificial immune systems (\ais) have been studied for over two decades and numerous algorithms inspired by immunological concepts have been developed and applied in
several application areas. Certain problem domains can be associated with immune processes more than others, such as anomaly detection, clustering/classification, virus detection or computer security. 
However, the existing applications of AIS are not limited to only these domains. Successful application of \ais has been seen in diverse areas which can seem to be not directly related to immunological functions such as robotics, image processing and optimisation~\cite{Dasgupta20111574}. There are a few examples of \ais applications to challenging real world problems or use within the industry but despite the successful applications of \ais the true potential of \ais has not yet been fully realized~\cite{Hart2008}. 

The opinion of some of the leading researchers in the field of \ais~\cite{Hart2008} has been that finding the right problem for \ais by associating the problem's features with \ais mechanisms, has been suggested as the way forward for \ais research in order to determine areas which can clearly benefit from immune inspired approaches. Following these guidelines, the research in this thesis explores a problem focused approach 
to present a novel \ais inspired by recent research in immunology, aimed towards multi-objective optimisation and provides an investigative experimental analysis on its behaviour and performance.  

The structure of this chapter is as follows: Section 1.2 describes the motivation behind the research in this thesis. It comprises of sub-sections which deal with the motivation behind studying the specific problem classes investigated during the research namely, the set cover problem, the knapsack problem and the single time-step dynamic set cover problem. This is followed by the key research questions addressed in this thesis in section 1.3. The key contributions made in this thesis are listed in section 1.4 and an overview of the chapters in this thesis is provided in section 1.5. The publications arising from this thesis are listed in section 1.6 and finally a summary is presented in section 1.7. 

\section{Motivation}

This thesis presents the development of a novel \ais called the \textit{Germinal centre artificial immune system} (\gcais) which has been designed by taking inspiration from recent immunological findings, specifically understanding of the workings of the germinal centre reaction~\cite{zhang2013germinal} in the immune system. Germinal centres are regions formed in the body which provide an immune response in order to eradicate specific invading pathogens which are not eliminated by the static immunity of the body which is non-specific and generic in nature~\cite{murphy2008janeway}. 

\subsection{The set cover problem and the immune system}\label{subsec:scpmotivation}
In simple terms the set cover problem (\scp) can be stated as: if one is given a set of items called the universe set and a collection of subsets of these items, the goal of the set cover problem is to cover all the items of the universe using the least number of subsets from the collection of subsets.

The motivation to study \scp for \ais research comes from the fact that the immune processes of fighting against pathogens,
in an abstract way can be seen as the immune system trying to to solve the \scp. At the onset of the disease caused by a pathogen invading the body, the immune system must prepare a counter attack towards the pathogen. One way it does so is by producing special cells called antibodies (Ab) which can bind themselves with the pathogen to eradicate it. Each pathogen has a special region called the \textit{active site} to which the antibodies can attach themselves via their binding sites, which can be seen as the . 
At the onset of the disease the antibodies may be poor and barely bind with the pathogen, but they undergo several iterations of improvements in an evolutionary manner, during which they begin to get better and start binding first weakly and then strongly with the antigens. It is only when the binding is strong enough that they can eradicate the pathogen. In other words the immune system wins versus the pathogen only when its antibodies can bind strongly with the pathogens.

The shared characteristics of the immune behaviour in relation to the set cover problem can be explained in the following principles. Assume that every pathogen in nature is an instance of the \scp problem which the immune system must address at a given time. The binding sites of the antibodies can be seen as potential solutions to the problem, i.e. a collection of subsets of the universe. These antibodies must improve from barely binding (covering only a part of the universe, possibly using many subsets) to weak binding (covering the whole universe but not with least subsets) to strong binding (covering the universe with the least subsets) to the pathogen's \textit{active sites} by the process of optimisation. Hence the immune system tries to solve the problem of finding the best matching between the antibodies and pathogens by randomised variations in the potential solutions \cite{Joshi2014}. 

Even though the set cover problem is a single objective problem with a constraint it can be converted to a multi-objective problem by using the constraint as a secondary objective.  According to~\cite{neumann2010bioinspired} this multi-objective formulation can be more efficient than the single objective version in finding the optimal solution, when used by an an evolutionary algorithm. At the same time the authors of~\cite{Hart2008} mention that the immune system has many goals suggesting their application for multi-objective optimisation. As an example, apart from the obvious task of fighting invading pathogens, the immune system also takes part in other processes such as assisting in wound healing, promoting the development of the organs of the immune system~\cite{DBLP:journals/complexity/Segel00}. This suggests that multi-objective optimisation as an application domain closely resembles the problems the artificial immune systems tackles.

More specifically germinal centre model of the immune system incorporates the two features that must be present in every multi-objective optimisation algorithm: guiding the search towards the global Pareto optimal region as well as maintaining diversity of solutions in the non-dominated front~\cite{DBLP:journals/ec/Deb99}. Guiding the search towards the optimal Pareto region can be seen in the germinal centres as the continuous process of improving the immune cells to fight against the invading pathogen. Maintaining diversity in the population can be seen during the communication and feedback of immune cells between germinal centres which provides selection pressure and helps maintain diversity as well as population size of the germinal centres. 

In order to follow a problem focused approach, first a problem must be selected that needs to be addressed. In this case the problem is the set cover problem and the motivation to design a new \ais stems from associating the \scp with the processes of the immune system that fight pathogens. Another attribute of the problem that motivates the use of \ais to tackle this problem are is the fact that a multi-objective formulation of the problem can be better than the single formulation as suggested in~\cite{neumann2010bioinspired} and as stated above \ais in general and germinal centre theory in particular has features that strongly resemble multi-objective optimisation. 

%Based on these arguments that \scp closely resembles the process of immunity and fighting against pathogens and that the fact a multi-objective formulation of \scp can be better than the single objective version multi-objective optimisation as a problem domain, to investigate the application of \gcais has been selected. Two combinatorial multi-objective optimisation problems are considered in this thesis namely the multi-objectivised \textit{set cover problem} (\scp) and the \textit{multi-objective d-dimensional knapsack problem} (\modkp). 


%It has been stated that the immune system is not particularly suited to perform single objective optimisation, for example solving a numerical or combinatorial single objective optimisation problem~\cite{Hart2008}. 
%The authors in~\cite{Hart2008} mention that the immune system has several goals which might even compete, as opposed to most optimisation problems which have a single objective. 
%As an example, apart from the obvious task of fighting invading pathogens, the immune system also takes part in other processes such as assisting in wound healing, promoting the development of the organs of the immune system~\cite{DBLP:journals/complexity/Segel00}. This suggests that multi-objective optimisation as an application domain closely resembles the problems the artificial immune systems tackles.
 


\subsection{A real multi-objective problem}
Though the initial motivation to design the \gcais stems from the need to tackle the multi-objective version of the \scp the resulting \ais designed is just like any other \ais for optimisation i.e. a is general purpose meta-heuristic. In order for the designed approach to be an effective meta-heuristic it must be tested on other multi-objective problems. Since the true goals of \scp are not multi-objective, a true multi-objective problem is needed for the validation of the effectiveness of the proposed algorithm. The multi-objective knapsack problem is an NP-hard combinatorial optimisation problem which is an extension of the knapsack problem using multiple knapsacks. One of the motivations for selecting to study this problem is the extensive work that has been done on this problem using multi-objective evolutionary algorithms (\moeas). The existing literature can provide both a healthy competition of approaches to solve the problem along with guidance on the design of experiments when performing empirical analysis of the \ais.

%Selecting this problem provides for a healthy competition of competing approaches which can provide guidance on experimental design when studying this problem.




\subsection{Memory in dynamic environments}

Real world problems often involve changes over time and previously known optimal solutions are usually not robust enough to find the new solution with relative ease for the changed problem~\cite{Hart2008}. The immune system is a dynamic and continuously adapting system which learns and maintains a memory of previously encountered diseases which makes memory and learning an indispensable feature of the immune system. Dynamic optimisation particularly dealing with a memory component is a suitable domain which closely resembles immune processes. Germinal centres are particularly suited for this application as they are responsible for the production of memory cells in the body during the course of the germinal centre reaction. This motivates the idea to incorporate memory in the proposed \gcais and understand the behaviour of the new approach. 

As an extension to the study of \scp, it is only fitting to examine a dynamic version of the problem with the motivation being that there is a practical relevance to studying the dynamic \scp as shown in~\cite{chrissis1982dynamic} where a model for dynamic facility location are proposed. Another practical application of dynamic \scp can be seen in crew scheduling for airlines or railways. The dynamics in these situations can arise due to variations in the number of rails/ aircraft or the number of crew available at different times.

Most of the work done on dynamic \scp along with techniques developed for evolutionary dynamic optimisation using memory approaches, focus on solving the problem in the dynamic setting and tracking the changing optimal values. 
Some work on memory techniques has shown the advantages of memory as being suitable in cases only when the changes recur. By extending the study on static SCP to incorporate a dynamic scenario, the goal is to find cases where using memory is useful and where it is harmful as well as a more fundamental question of why does memory behave in this fashion given these cases. To the best of our knowledge this will is the first time a memory based
approach has been used to tackle the dynamic \scp.

\section{Research questions}

The following key questions considered in this thesis clarify the main objectives of this research. These questions are presented again in the following chapters where
they are addressed along with a discussion on the answers obtained.

The biological immune system's task of fighting against diseases by creating and remembering of antibodies can be modelled as the set cover problem in combinatorial optimisation. Based on the knowledge of recent research in the understanding of the germinal centre reaction in the biological immune system, since the \scp is the key motivating problem behind the design of a new \ais, a key question arises: 
\begin{quote}
   \textit{Is it possible to model the recent understanding of the \GC reaction into an immune system that tackle the multi-objective \scp?}
%	\textit{Can a good artificial immune system be designed using inspiration from the recent understanding of the \GC reaction?}
\end{quote}
 The algorithm design process must abstract processes from the \GC reaction that are suitable for multi-objective optimisation and must be tested on multi-objective problems to analyse its performance. This question is addressed in chapter 4 where 
 the \gcais is presented. An abstract model of the immune processes in the germinal centre reaction is created by carefully studying its components. This abstract model is then converted to an artificial 
 immune system which is presented as the \gcais algorithm.

Given that the biological immune system like any natural process is robust and performs well, it will be interesting to answer the following query: 
\begin{quote}
\textit{How does the novel artificial immune system perform on the set cover problem?}
\end{quote}
An experimental analysis on the performance \gcais is presented on in chapter 4 on the \scp where it is compared with a simple multi-objective evolutionary algorithm called \pgsemo. Run time evaluation of solution quality versus elapsed time as evaluations as well as communication effort are used as the criteria for comparison.

Moreover, since the designed \ais is a meta-heuristic that can be applied to multi-objective problems, after studying the performance on \scp it is well worth to investigate:
 
\begin{quote}
\textit{How does the novel algorithm perform on a real multi-objective optimisation problem such as the multi-objective knapsack problem?}
	
\end{quote}

In the same chapter, along with the experiments on the set cover problem, \gcais is also compared with a popular multi-objective optimisation algorithm (\nsga) on several instances of the \modkp. For this study 3 prominent metrics from multi-objective optimisation are utilised, namely: hypervolume, diversity and generational distance. Potential strengths and weaknesses of the algorithm are then observed based on the experiments.

The studies on \scp and \modkp provide an analysis on the behaviour of \gcais and these studies lead to another key question: 
\begin{quote}
	\textit{What can be learned about the strengths and weaknesses of the \gcais based on these studies?} 	
\end{quote}
The key strength of the algorithm which is observed as a result of the experimental analysis in chapter 4 is that it does not require any parameter tuning if the mutation rate remains fixed. This can be very crucial as parameter tuning is an extremely important process to obtain good performance from evolutionary algorithms. At the same time, population size explosion is identified as the key weakness of the algorithm, which is observed in experiments with the knapsack problem when the dimensionality of the instances increased. This is a weakness as it can lead of wastage of available fitness evaluation in explored areas as well as causing challenges for the decision maker to decide on a final solution to the problem.

This is an important question as it helps in understanding the design decisions behind the algorithms which follows into: 
\begin{quote}
	\textit{Knowing certain weaknesses in \gcais, how can improvements be made to overcome the shortcomings?} 
	
\end{quote}

The weakness of the algorithm are addressed in chapter 5 where an improved variant of the \gcais is presented by modifying the domination strategy. The well known concept of $\epsilon$-dominance is incorporated into \gcais to handle population explosion, which replaces the original dominance relation. Though originally proposed for promoting diversity and convergence in populations, this technique has been used for maintaining population size in algorithms.

These improvements in \gcais leads to the testing, analysis and evaluation cycle on the \scp and \modkp to determine if the improvements have been successful.

Dynamic optimisation problems involve finding and tracking the changing optimal solution. Knowing that memory and learning are important components of the immune system
a key question to investigate is: 
\begin{quote}
	\textit{When using \gcais for dynamic optimisation of \scp, does using solutions from memory perform better than starting from scratch ?} 
		
\end{quote}


Upon closer inspection of this question it can be further broken
down into a more fundamental query: 
\begin{quote}
	\textit{When does using memory perform better than a solution from scratch and when does it fail? why does this behaviour occur?}
	
\end{quote}


These questions are addressed in chapter 6, where a memory component is added to the \gcais and this variant called the \mgcais is analysed in a dynamic setting of the set cover problem. A model to introduce different kinds of changes in static set cover problem is introduced and a single time step dynamic version of the set cover problem is formulated. Using this problem the \gcais is compared with \mgcais in order to discern the usefulness of memory in different versions of the dynamic set cover problem.




\section{Contributions of the thesis}

By providing answers to the research questions asked above the following contributions are made in this thesis:

\begin{enumerate}
	\item \textbf{A novel \ais designed for multi-objective optimisation.}\\
	By condensing ideas from the germinal centre theory that are relevant and enticing for multi-objective optimisation a novel \ais is proposed to tackle the \scp. Several algorithms exists in the field of \ais, each with its own strengths and weaknesses. The \gcais, presented in this thesis is a simple yet effective algorithm for solving the \scp, that can be applied for multi-objective optimisation, which has enticing properties such a small number of parameters which makes it an appealing algorithm for researchers in the field of \ais and multi-objective optimisation.
	
	\item \textbf{An investigation on the performance of \gcais on \scp and \modkp}\\
	A performance analysis which sheds light on the behaviour of \gcais when considering the \scp and \modkp is presented. Performance benefits over competitors are elucidated as well as a key weaknesses is identified. This is one of the very few experimental works on the multi-objective setting of \scp. Previous works have been limited to single objective scenarios or theoretical studies.  
	
	\item \textbf{An improved variant of \gcais}\\ Weaknesses identified in \gcais are explored and tackled. An improved variant of the \gcais called $\epsilon$-\gcais is proposed. A popular technique from multi-objective optimisation which is mostly used for introducing diversity as well as convergence in \moeas is incorporated in $\epsilon$-\gcais. The advantages of the techniques include improving diversity and convergence as well as overcoming the weaknesses in \gcais.
	
	\item \textbf{Investigative analysis of $\epsilon$-\gcais on \scp and \modkp} \\
	Analysis on the performance of $\epsilon$-\gcais is performed by a comparative study with a state of the art algorithm for multi-objective optimisation on \scp and \modkp. 
	
	\item \textbf{Understanding the effects of memory in a dynamic scenario.} \\ Using memory may not always be useful when considering a dynamic setting. This phenomenon is well known in biological immune systems and research exists which describes its causes. As a consequence, scenarios when and where using memory is useful for dynamic \scp and what causes memory to behave in this manner is presented.
	
	
\end{enumerate}


\section{Overview}
The first three chapters of the thesis introduce relevant problems and concepts and serve as a background for the remaining thesis. The core of the thesis is contained in Chapters 4 through 6 which follow progressively to answer the research questions. Though each core chapter is somewhat self-contained, the thesis can be best appreciated when read in its entirety. 

A background of concepts important to the thesis along with a literature review of the relevant work done in the field is provided in Chapter 2. Artificial immune systems are introduced and some important existing models of the immune system are discussed. A review of the key work done on the different models of the immune system is provided. Multi-objective optimisation along with dynamic evolutionary optimisation is introduced, covering the formal introduction of the set cover problem and the multi-objective d-dimensional knapsack problem. 
A literature view of relevant work on the set-cover problem and d-dimensional knapsack problem in the multi-objective context is presented along with a review of memory based techniques for dynamic optimisation followed by relevant work done on the dynamic set cover problem. These problems form the test bed for the empirical experiments in this thesis.

The methodology used for the experimentation and analysis of results are detailed in Chapter 3. The multi-objective algorithms used for comparison with \gcais are presented in detail followed by a description of the problem instances which are used for the experimentation. The stopping criteria for the various algorithms is discussed followed by the technique used for reference front generation employed in this thesis. Finally the metrics of comparison employed for the analysis of the results are detailed followed by a brief description of the statistical tests used in this thesis.  

Chapter 4 introduces the \gcais algorithm. The design of the \gcais is explained making abstractions from the theory of the \GC reaction. This is followed by an evaluation of \gcais by comparative experimental studies on the \scp and \modkp. 

Chapter 5 tries to remedy the shortcomings of \gcais observed in Chapter 4. $\epsilon$-dominance is introduced as a method to solve the weaknesses of \gcais and is incorporated in the variant of \gcais called \egcais. The performance of \egcais is evaluated on \scp and \modkp by comparing with some state-of-the-art algorithms.

The effects of learning are investigated in Chapter 6, where a memory component is introduced in \gcais, called the \mgcais. Two models for dynamic \scp are introduced in detail along with methodology for instance creation for these models alongside motivations and practical applications of the models. Questions such as when and why is using memory useful in the case of the dynamic \scp are considered. 

The thesis is concluded in Chapter 7 with a discussion of the contributions and implications along with some possible directions of future work.

\section{Publications associated with this research}

A list of the refereed publication arising from the research conducted as part of this this is presented below. In addition, the relationship between the thesis chapters and the work presented in these publications is mentioned along with a statement of my contribution in these publications.

\begin{enumerate}
\item \bibentry{DBLP:conf/ppsn/JoshiRZ14}. The work in this paper is featured in chapter 4. For this paper I was responsible for the majority of the work, which was comprised of experimentation, analysis of the results and writing.
\item \bibentry{springerlink:10.1007/978-3-319-16468-7_10}. The work in this paper is featuyred in chapter 4 and chapter 5. For this paper I was responsible for the majority of the work, which was comprised of experimentation, analysis of the results and writing.	
\item \bibentry{Joshi2015mic} The work in this paper is featured in chapter 6. For this paper I was responsible for the majority of the work, which was comprised of experimentation, analysis of the results and writing.
\end{enumerate}

\section{Summary}
This chapter sets the stage for the motivations and ideas behind the research presented in this thesis. The key problems addressed in this thesis are identified and a connection between the problems is made with the principles and properties of the immune system which serves as the motivation to design and test a new \ais algorithm for these problem domains. The key research questions addressed in this thesis are discussed along with pointers to Chapters where they are discussed. The contributions made by the research in this thesis are listed in a consolidated form along with an overview of the structure of the thesis. Finally the publications arising from the work done towards this thesis are mentioned along with the contribution of the author towards these publications.
