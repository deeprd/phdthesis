
%%%%%%%%%%%%%%%%%%%%%%% file typeinst.tex %%%%%%%%%%%%%%%%%%%%%%%%%
%
% This is the LaTeX source for the instructions to authors using
% the LaTeX document class 'llncs.cls' for contributions to
% the Lecture Notes in Computer Sciences series.
% http://www.springer.com/lncs       Springer Heidelberg 2006/05/04
%
% It may be used as a template for your own input - copy it
% to a new file with a new name and use it as the basis
% for your article.
%
% NB: the document class 'llncs' has its own and detailed documentation, see
% ftp://ftp.springer.de/data/pubftp/pub/tex/latex/llncs/latex2e/llncsdoc.pdf
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\documentclass[runningheads,a4paper]{llncs}
\usepackage{listings}
\usepackage{caption}
\usepackage{amssymb}
\usepackage{amsmath}
\setcounter{tocdepth}{3}
\usepackage{graphicx}
\usepackage{algorithm} 
\usepackage{algorithmic}
\newtheorem{defi}{Definition}
\usepackage{booktabs}
\usepackage{url}
\urldef{\mailsa}\path|{axj006, j.e.rowe, c.zarges}@cs.bham.ac.uk| 
\newcommand{\keywords}[1]{\par\addvspace\baselineskip
\noindent\keywordname\enspace\ignorespaces#1}

\begin{document}

\mainmatter  % start of an individual contribution

% first the title is needed
\title{Improving the Performance of the Germinal Center Artificial Immune System using $\epsilon$-dominance : A Multi-objective Knapsack Problem Case Study }

% a short form should be given in case it is too long for the running head
\titlerunning{Improving the performance of GC-AIS using $\epsilon$-dominance}

% the name(s) of the author(s) follow(s) next
%
% NB: Chinese authors should write their first names(s) in front of
% their surnames. This ensures that the names appear correctly in
% the running heads and the author index.
%
\author{Ayush Joshi \and Jonathan E. Rowe \and Christine Zarges}
%
%\authorrunning{Performance of an Improved Artificial Immune System on the Multi-objective Knapsack Problem}
% (feature abused for this document to repeat the title also on left hand pages)

% the affiliations are given next; don't give your e-mail address
% unless you accept that it will be published
\institute{School of Computer Science, University of Birmingham,\\
Edgbaston, Birmingham, UK\\
%\\
\mailsa\\
}

%
% NB: a more complex sample for affiliations and the mapping to the
% corresponding authors can be found in the file "llncs.dem"
% (search for the string "\mainmatter" where a contribution starts).
% "llncs.dem" accompanies the document class "llncs.cls".
%

\toctitle{Lecture Notes in Computer Science}
\tocauthor{Authors' Instructions}
\maketitle


\begin{abstract}
The Germinal center artificial immune system (GC-AIS) is a novel immune algorithm inspired by recent research in immunology, which requires very few parameters to be set by hand. The population of solutions in GC-AIS is dynamic in nature and has no restrictions on its size which can cause problems of population explosion, where the population keeps growing very rapidly, leading to wasteful fitness evaluations. In this paper we try to address this problem in the GC-AIS by incorporating $\epsilon$-dominance, which is a well known mechanism in multi-objective optimization to regulate population size. The improved variant of GC-AIS is compared with a well known multi-objective evolutionary algorithm NSGA-II on the multi-objective knapsack problem. We show that our improved GC-AIS performs better than NSGA-II on the instances of the knapsack problem taken from \cite{zt1999a} inheriting the same benefits of having to set fewer parameters manually.


\keywords Artificial Immune Systems, \textsc{GC-AIS}, \textsc{NSGA-II}, Knapsack Problem
\end{abstract}


\section{Introduction}

Multi-objective optimisation is the task of finding optimal solutions to a 
problem which has several objectives, that often compete with each other. Hence, there exists a set of optimal 
solutions to the problem called the pareto optimal set. Real life problems often involve competing objectives and the complexity of these problems can cause problems when applying exact methods \cite{DBLP:journals/tec/Jaszkiewicz02}. Evolutionary algorithms have been employed to solve multi-objective optimization problems since the mid-1980s \cite{DBLP:conf/icga/Schaffer85a} with good success. 

Artificial immune systems (AIS) are meta-heuristics which have been developed taking inspiration from different models of the immune system of vertebrates. The immune system is an especially interesting biological system as it possesses several desirable properties combined together. Due to features like diversity, robustness,
and memory, AIS have been applied to a large number of applications such as machine learning, security, robotics, optimization Castro and Timmis \cite{de2002artificial} provide a detailed survey of applications. Compared with other optimization algorithms AIS are relatively new, and a survey of AIS that have been applied to solve multi-objective optimization problems is provided by Freschi et al.  \cite{freschi2009multiobjective}. 

Natural processes perform several complicated tasks with efficiency and tend to be robust. Current approaches
used to solve real world problems are facing problems with robustness and scalability \cite{greensmith2007dendritic,kim2001towards}, as real world problems become more and more complex. Therefore it is a growing trend that understanding and using more detailed ideas from natural processes can help us design better performing systems. Towards this some work has been done 
by Greensmith et al. \cite{greensmith2006articulation} but this has mostly been limited to intrusion detection and classification. Sim et al. \cite{sim2014lifelong} have proposed a hyper-heuristic called NELLI, which learns from changing problem 
landscapes, and has been shown to perform better than single human-designed heuristics.

GC-AIS is a novel AIS introduced by Joshi et al. \cite{DBLP:conf/ppsn/JoshiRZ14}, which is inspired by recent research on the Germinal center reaction \cite{zhang2013germinal}. It has interesting properties like dynamic population size and it requires very few parameters to be manually specified. In each generation of the GC-AIS every solution creates a mutated clone which can potentially cause population explosion. This characteristic can sometimes cause problems as many fitness evaluations are wastefully expended without observing significant improvements in solutions. 

In this paper we replace the dominance comparison in the GC-AIS with $\epsilon$-dominance as a method to control population size. This modified GC-AIS which we call $\epsilon$-GC-AIS is compared  with the non-dominated sorting genetic algorithm-II (NSGA-II) \cite{deb2000fast} on instances of the multi-objective  d-dimensional knapsack problem (MOd-KP) taken from \cite{zt1999a}. Three measures used in multi-objective optimization, namely hypervolume \cite{zitz1999a}, generational distance \cite{van1999multiobjective} and generalized spread \cite{DBLP:conf/cec/ZhouJZST06} are used as metrics for comparison.
It is shown that with a suitable choice of $\epsilon$ value, $\epsilon$-GC-AIS performs better than NSGA-II and has the inherent benefit of requiring less parameters to be set manually.

The outline of the paper is as follows: In Section 2 multi-objective optimization is introduced along with a detailed description of the multi-objective knapsack problem. Section 3 gives the description of the GC-AIS model along with 
a description of the  $\epsilon$-GC-AIS algorithm. In Section 4 the experimental setup is explained and along with the obtained results in Section 5. The paper is concluded in Section 6 with a discussion on the observed results and conclusions thereafter. 


\section{Preliminaries}

\subsection{Multi-objective optimization}
Multi-objective optimization can be described as finding solutions to problems which have more than one objective, that are often in competition with each other. If we denote the fitness of the different objectives as $f_i$ $(1 \leq i \leq n)$, then a solution $x^{1}$ dominates another solution $x^{2}$ if $\forall i \: f_i(x^1) \geq f_i(x^2)$ and there exists at least one $i$ such that $f_i(x^1) > f_i(x^2) $, assuming maximization without loss of generality. At any time during its run, a multi-objective optimization algorithms may maintain a set of non-dominating solutions to the problem. If for some solution $x$ there exists no other solution
$y$ that dominates $x$, then $x$ is a pareto optimal solution to the problem. A pareto
optimal set is a set containing all pareto optimal solutions.

\subsection{The multi-objective d-dimensional knapsack problem}
The knapsack problem is a widely studied NP-hard combinatorial optimization problem \cite{zt1999a} 
with real world applications like capital budgeting and resource allocation  \cite{DBLP:journals/eor/ShahR11}. 
The single objective knapsack problem consists of a set of items which have associated weights and profits, and 
a knapsack which has a fixed capacity. The goal is to find the set of items which can be packed in the knapsack
giving the maximum profit without exceeding the capacity of the knapsack. By introducing multiple knapsacks this
single objective problem can be extended to a multi-objective version. Each knapsack has its own capacity and the 
items have different profits and weights associated with each knapsack. A practical example of this problem is packet scheduling for wireless networks with relay nodes \cite{cohen2014multi}.

The solutions to the multi-objective d-dimensional knapsack problem (MOd-KP) can be encoded as bit strings of length $m$ where $m$ is the total number of items available. A $1$ indicates the presence of an item while $0$ indicates the absence of the item. It should be noted that if a bit is $1$ then an item is considered to be present in 
all the knapsacks and vice-versa. A more formal definition of the multi-objective 
knapsack problem can be stated as:

%
%\begin{defi}
%Given a set of $m$ items and $n$ knapsacks, and associated with each knapsack
%\[
%p_{i,j} = \text{profit of item i w.r.t knapsack j}\]
%\[w_{i,j} = \text{weight of item i w.r.t knapsack j} \]
%\[c_{j} = \text{capacity of knapsack j}
%\]
%let a solution be represented as 
%\[x=(x_{1},x_{2},\cdots,x_{m}) \text{ where } x_{i} \in \{0,1\} \]
%maximize \[f(x) = (f_{1}({x}),f_{2}({x}),f_{3}({x}),...,f_{n}({x})) \text{ where } f_{i}({x}) = \Sigma_{j=1}^{m}p_{i,j}\cdot x_{i}\]
%subject to \[ \Sigma_{j=1}^{n}w_{i,j}\cdot x_{j} < c_{i}; \forall i \in \{1,2,3,...n\} \].
%\end{defi}

\begin{defi}
Let $m$ denote the number of items and $n$ the number of knapsacks. Let $p_{i,j}$ and $w_{i,j}$ the profits and
weights of item i with respect to knapsack j, respectively, and $c_j$ the capacity of knapsack j.  
 Let a solution be represented as $x=(x_{1},x_{2},\cdots,x_{m}) \text{ with } x_{i} \in \{0,1\} $. The objective is to
maximize \[f(x) = (f_{1}({x}),f_{2}({x}),\cdots,f_{n}({x})) \text{ where } f_{i}({x}) = \Sigma_{j=1}^{m}p_{i,j}\cdot x_{i}\]
subject to $ \Sigma_{j=1}^{n}w_{i,j}\cdot x_{j} < c_{i}; \forall i \in \{1,2,3,...n\} $.
\end{defi}

The MOd-KP is a constrained multi-objective problem where the constraint is the capacity of the knapsacks.
Not all possible bit string combinations represent valid solutions and some repair mechanism must be applied in order to transform invalid solutions to valid ones. In \cite{zt1999a}, the authors used the maximum profit by weight ratio (greedy repair) method to repair invalid solutions. For an item $i$, the maximum profit by weight ratio is given by: 
\begin{equation}
  q_{i} = \mbox{max}(p_{i,j}/w_{i,j}), \quad  \forall j \in (1,2,...,n)\enspace .
\end{equation}

An item with the lowest maximum profit by weight ratio is removed first, and they are removed iteratively in an increasing order of the ratio,
 until a feasible solution is obtained. 

Another repair heuristic was introduced in \cite{DBLP:journals/tec/Jaszkiewicz02}  where the weighted profit by weight ratio (weighted scalar repair),  is used to find the order of removal of items. In this approach the items are sorted based on:
\begin{equation}
q_{i} =  \Big(\sum_{j=1}^{n}  \lambda_{i}p_{i,j}\Big) / \Big(\sum_{j=1}^{n} w_{i,j}\Big), \quad  \forall j \in (1,2,...,n)\enspace . 
\end{equation}
where the $\lambda_i$ are the scalar coefficients of the linear utility function used for scalarizing the multi-objective fitness vector
in the Multi-objective genetic local search (MOGLS)\cite{DBLP:journals/tec/Jaszkiewicz02} algorithm. These coefficients are generated randomly at each generation of MOGLS and 
are used for selection as well as repair. The generation procedure for normalized weight vectors is given in Algorithm \ref{algo:weight}.
 

\begin{algorithm}
\caption{Algorithm for generation of normalized weight vectors. rand returns a random number between 0 and 1. \cite{DBLP:journals/tec/Jaszkiewicz02}}\label{algo:weight}
\begin{algorithmic}
\STATE $\lambda_{1} = 1 - \sqrt[n-1]{rand()}$
\STATE $\dots$
\STATE $\lambda_{j} = (1-\sum_{i=1}^{j-1}\lambda_{i})\cdot(1-\sqrt[j-1]{rand()})$
\STATE $\dots$
\STATE $\lambda_{n} = 1-\sum_{i=1}^{n-1}\lambda_{i}$
\end{algorithmic}
\end{algorithm}


\section{The \textsc{ $ \epsilon$-GC-AIS} Algorithm }

The $\epsilon$-GC-AIS is an improved variant of GC-AIS originally proposed by Joshi et al. \cite{DBLP:conf/ppsn/JoshiRZ14}, which is a new AIS designed using knowledge from cutting edge research in immunology, specifically the understanding of the germinal center (GC) reaction \cite{zhang2013germinal}. Germinal centres are regions in the immune system where B cells a type of immune cell that generates antibodies ($Abs$) to fight an infection, are presented with the invading pathogens~\cite{murphy2011janeway}. 

The key highlights of the GC reaction are as follows: At the start of the invasion the number of GCs grows and they try to find the best \textit{Ab} for the pathogen by continuously mutating and selecting the B cells which can bind with the
pathogen. There is periodic communication between GCs by transmitting $Abs$. By proliferation, mutation and selection of B cells this reaction is able to produce $Abs$ which can eradicate the pathogen. Towards this stage the number of GCs starts declining. The new theory proposed in \cite{zhang2013germinal} deals with the selection aspect of this reaction, where there is a direct competition between \textit{Abs} and mutating B cells, and the cells which are unable to compete die by apoptosis (cell death). Entire GCs may disappear if the B cells within them cannot compete with \textit{Abs} from neighbouring GCs.

\begin{algorithm}
\caption{The \textsc{$\epsilon$-GC-AIS}}\label{algo:GC-AIS}
\begin{algorithmic}
\STATE \textbf{Let} $G^t$ denote the population of GCs at generation $t$ and $g^{t}_{i}$ the $i$-th GC in $G^t$.
\STATE Create GC pool $G^0= \{g_{1}^{0}\}$ and initialise $g_{1}^{0}$. Let $t: = 0$.
\LOOP
\FOR{each GC $g^{t}_{i}$ in pool $G^t$ in parallel}
\STATE Generate random weight vectors $\lambda_{i}$ \ref{algo:weight}.
\STATE Create offspring $y_i$ of individual $g^{t}_{i}$ by standard bit mutation.
\STATE Repair invalid offsprings using the weighted scalar repair approach.
\ENDFOR
\STATE Add all $y_i$ to $G^{t}$, remove all $\epsilon$-dominated solutions from $G^{t}$ and let $G^{t+1}=G^{t}$.
\STATE  \textbf{Let} $t=t+1$.
\ENDLOOP
\end{algorithmic}
\end{algorithm}

Based on Algorithm \ref{algo:GC-AIS} a brief description of $\epsilon$-GC-AIS is as follows. The \textsc{$\epsilon$-GC-AIS} starts with one GC which contains one individual that represents a B cell. Offspring are created by standard bit mutation of B cells in GCs. Standard bit mutation means that each bit can be flipped with the probability $1/n$. In every generation there is a migration of fitness values of the offspring between GCs, which corresponds to the migration of $Ab$ between GCs. After migration, $\epsilon$-dominated solutions are deleted which correspond to the eradication of GCs. The surviving offspring correspond to new GCs. This leads to a model where the number of GCs is dynamic in nature. The difference in this modified GC-AIS from the original model is the incorporation of $\epsilon$-dominance which replaces the previous standard dominance relation. The \textsc{$\epsilon$-GC-AIS} always maintains a set of $\epsilon$-non-dominated solutions in every generation and the population size of GCs is dynamic in nature.

$\epsilon$-dominance \cite{laumanns2002combining} is a generalization of the dominance relation, which can be conceptually visualized for the 
two dimensional case as follows: the objective space is divided into a grid of rectangles of dimensions $\epsilon$ and solutions are mapped onto this grid. Assuming maximization, if two solutions are in different boxes then they are compared using the standard dominance relation, but if they are in the same box then they are compared based on their euclidean distance from the origin, and the solution with the larger distance is kept. Figure \ref{epsfig} gives a diagram of the $\epsilon$ dominated front, where the red circles represent the dominated points and the green circles represent the point on the $\epsilon$ dominated front.

\begin{figure}
\center
\includegraphics[trim = 0mm 0mm 0mm 8mm, clip, scale=0.6]{eps.pdf}  
\caption{Diagram of $\epsilon$ dominated front along with dominated points }  \label{epsfig}
\end{figure}


\section{Experimental setup}

In this section we describe the experimental setup used in this study: \textsc{$\epsilon$-GC-AIS} and 
the \textsc{NSGA-II} are compared by testing them on some benchmark instances of the multi-objective knapsack problem. A total of 12 instances
are provided in \cite{zt1999a} and are grouped into 3 classes based on the number of knapsacks. Each class consists of 4 instances
based on the number of total items. The different knapsack numbers are 2, 3 and 4 and the number of items are 100, 250, 500 and 750. 
30 independent runs each with one million fitness evaluations were performed for each algorithm and statistical results were recorded. Any-time data and end-of-run performance metrics are provided.

NSGA-II \cite{deb2000fast} is a widely used elitist multi-objective evolutionary algorithm which uses non-dominated sorting to rank solutions according to levels of dominated fronts, as well as crowding distance measure to maintain diversity in the solution set. In each generation a fixed population size $N$ is maintained which is initialized by the user, individuals are selected by tournament selection using the ranks and crowding distance and are then subject to crossover and mutation to create $N$ offspring. These offspring are combined with the parent population and the best $N$ solutions are carried to the next generation based on the crowding distance measure and non-domination.

\begin{table}
\protect\caption{Algorithm settings for GC-AIS and NSGA-II}\label{parameters}
%\renewcommand{\arraystretch}{1.5}
\centering
\begin{tabular}{ccc}
\hline 
Algorithm parameters & NSGA-II & $\epsilon$-GC-AIS\tabularnewline
\hline 
Initial Population & Refer Table-2 & 1\tabularnewline
%\hline 
Selection & Tournament & All reproduce\tabularnewline
%\hline 
Mutation Type & Standard bit mutation & Standard bit mutation\tabularnewline
%\hline 
Mutation Probability & 1 & 1\tabularnewline
%\hline 
Mutation rate & 5/n & 1/n\tabularnewline
%\hline 
Crossover Type & One point crossover & -\tabularnewline
%\hline 
Crossover Probability & 0.8 & -\tabularnewline
%\hline 
Epsilon Value & - & 20/50\tabularnewline
\hline 
\end{tabular}

\end{table}

In the work by Zitzler et al.~\cite{zlt2001a}, authors state that NSGA-II and SPEA2 have similar behaviour on different problems considered in their work which includes MOd-KP, where they have used the greedy approach for repair. Ishibuchi and Kaige \cite{ishibuchi2003effects} show that the weighted scalar repair approach improves diversity of solutions and also increases convergence speed in many cases. It is also shown that performance of multi-objective algorithms strongly depends on the choice of the repair procedure used which is confirmed by work done in \cite{DBLP:journals/eor/ShahR11}. The implementation of NSGA-II in this paper closely resembles that of the work in \cite{ishibuchi2003effects}, and the algorithm is kept as close to its pure form as possible with the inclusion of the weighted scalar repair. One random weight vector is generated for every solution which needs repair. In this work the implementation of NSGA-II has been adapted from the MOEA framework\footnote{www.moeaframework.org}.

The \textsc{$\epsilon$-GC-AIS} algorithm requires only two parameters to be set by hand, the mutation rate and a value for $\epsilon$,
on the other hand NSGA-II requires three parameters to be set, namely population size, the mutation rate and probability of crossover. For our experiments these settings are similar to the ones used by \cite{DBLP:journals/eor/ShahR11} as our implementation of NSGA-II is closest to their $\epsilon$-NSGA-II where all the parameters values have been kept as close to the original work in \cite{zt1999a,zlt2001a}. The mutation rate for NSGA-II has been set to $5/n$ according to the guidelines in \cite{laumanns2001effects}. The mutation rate is the probability with which every bit is flipped per individual, while mutation probability refers to the probability of each individual to be mutated. The parameter values can be seen in Table \ref{parameters}. Since population size for NSGA-II is usually set according to the problem instance, the values again have been selected from \cite{DBLP:journals/eor/ShahR11} and are depicted in Table \ref{Population}.    

\begin{table}
\centering
\protect\caption{Population size used in NSGA-II for different instances of MOd-KP }\label{Population}
\begin{tabular}{cccc}
\hline 
 &  & Knapsacks & \tabularnewline
\cline{2-4} 
Items & 2 & 3 & 4\tabularnewline
100 & 150 & 200 & 250\tabularnewline
250 & 150 & 200 & 250\tabularnewline
500 & 200 & 250 & 300\tabularnewline
750 & 250 & 300 & 400\tabularnewline
\hline 
\end{tabular}

\end{table}

For the purpose of comparison three popular measures used in evolutionary multi-objective optimization, the hypervolume metric \cite{zlt2001a}, generational distance \cite{van1999multiobjective} and the generalized spread metric \cite{DBLP:conf/cec/ZhouJZST06}, have been used. The spread metric is a measure of how the solutions are distributed in the non-dominated front. This measure was proposed for problems with two dimensions in \cite{deb2000fast} but was later generalized for any number of dimensions in the work by \cite{DBLP:conf/cec/ZhouJZST06}. The lower the value for the metric the better the spread. The hypervolume metric is a measure of the volume dominated by the achieved non-dominated set with respect to a nadir point, and higher values of this metric are considered better. The generational distance is a method to estimate how far the solutions in the non-dominated set are from the true pareto set, and lower values for this metric are considered to be better.

\begin{figure}
\includegraphics[trim=0mm 85mm 0mm 75mm,clip,width=1\textwidth]{5002new.pdf}  
\caption{Any-time plots for $\epsilon$-GC-AIS and NSGA-II. X axes show the generations, Y axes show the measure of the respective metrics. Instance size 500 items, 2 knapsacks, Hypervolume and Generational distance are plotted on log scale.}  \label{plots}
\end{figure}

The implementations of these metrics have been adapted from the Jmetal framework \cite{DN11} where, for generational distance and spread, the non-dominated front as well as known best reference fronts are required. The authors of \cite{zt1999a} have provided the pareto fronts for only 4 instances, namely the 2 knapsack instance with items 100, 250 and 500 and 3 knapsack with items 100. Therefore, the technique employed in \cite{DBLP:journals/eor/ShahR11} has been used to generate best-known non-dominated fronts for the remaining instances. According to this method, $\epsilon$-GC-AIS and NSGA-II were run for 10 independent runs each for 1 million fitness evaluations. The final non-dominated fronts from each run were combined and all dominated solutions were removed. The resulting fronts were used as reference fronts for the generation of performance measures.   


\section{Results and Discussion}

The first set of experiments is performed to find a suitable value for $\epsilon$. Five different values of $\epsilon$ were tried namely 2, 5, 10, 15, and 20 along with standard GC-AIS with dominance without $\epsilon$ and these were compared with NSGA-II. Run-time plots 
of population, hypervolume, generational distance and spread are provided in Figure \ref{plots}. 30 independent runs were performed for each setting and plots of the mean of population, hypervolume, generational distance and spread metric for each generation averaged over 30 runs were plotted. A representative plot for the two dimensional problems is provided here. Including curves for all $\epsilon$ values was not possible in Figure \ref{plots}, and selected values of 10, 20, along with NSGA-II and standard GC-AIS ($\epsilon = 0$) are shown. 

%
%\begin{figure}[tbh]
%%[trim=0mm 70mm 0mm 70mm,clip,width=1\textwidth]
%\centerline{\includegraphics[scale=0.4]{untitled.eps}}  
%\caption{Fitness plots for \textsc{GC-AIS} and \textsc{PGSEMO} for problem $scp41$ with standard deviation as shaded error bars, averages performed over 30 independent runs.} \label{plots}
%\end{figure}


\begin{table}
\protect\caption{Hypervolume measure values for NSGA-II and $\epsilon$-GC-AIS}\label{hypervol}
\centering
\def\arraystretch{1.2}
\begin{tabular}{|c|c|c|c|c|c|c|}
\hline 
\multicolumn{1}{|c}{} &  & \multicolumn{1}{c}{$\epsilon$-GC-AIS} &  & \multicolumn{1}{c}{NSGA-II} &  & \tabularnewline
\hline 
Sacks & Items & average & std.dev & average & std.dev & Wilcoxon test\tabularnewline
\hline 
2 & 100 & 1.68e+7 & 6.26e+4 & 1.68e+7 & 5.37e+4 & 0.137\tabularnewline
\hline 
 & 250 & \textbf{9.59e+7} & 3.57e+5 & 9.51e+7 & 3.85e+5 & 5.92e-8\tabularnewline
\hline 
 & 500 & \textbf{3.94e+8} & 1.08e+6 & 3.83e+8 & 1.72e+6 & 3.01e-11\tabularnewline
\hline 
 & 750 & \textbf{8.51e+8} & 3.12e+6 & 8.19e+8 & 3.24e+6 & 3.01e-11\tabularnewline
\hline 
3 & 100 & 6.11e+10 & 4.68e+8 & 6.08e+10 & 3.79e+8 & 0.079\tabularnewline
\hline 
 & 250 & \textbf{8.46e+11} & 6.29e+9 & 8.29e+11 & 5.28e+9 & 1.32e-10\tabularnewline
\hline 
 & 500 & \textbf{6.56e+12} & 3.87e+10 & 6.42e+12 & 3.89e+10 & 3.33e-11\tabularnewline
\hline 
 & 750 & \textbf{2.19e+13} & 1.19e+11 & 2.15e+13 & 1.80e+11 & 3.01e-11\tabularnewline
\hline 
4 & 100 & 1.51e+14 & 2.77e+12 & \textbf{1.63e+14} & 1.17e+12 & 3.01e-11\tabularnewline
\hline 
 & 250 & 6.33e+15 & 6.78e+13 & \textbf{6.44e+15} & 6.15e+13 & 1.15e-7\tabularnewline
\hline 
 & 500 & 9.72e+16 & 9.81e+14 & 9.72e+16 & 1.05e+15 & 0.371\tabularnewline
\hline 
 & 750 & 4.70e+17 & 3.91e+15 & \textbf{4.82e+17} & 4.80e+15 & 4.61e-10\tabularnewline
\hline 
\end{tabular}

\end{table}

It can be seen from Figure \ref{plots} that early in the run the GC-AIS obtains better hypervolume than NSGA-II using all values of $\epsilon$, and NSGA-II catches up around the middle. The plots for spread measure show that using different $\epsilon$ values can cause variations in the spread. It can be seen from the plots of hypervolume and generational distance that $\epsilon$-GC-AIS achieves better values in the early phase of the runs, while NSGA-II takes some time to catch up. Also, the plots for the population size show that $\epsilon$ value has a big impact on the population size. Since we are interested in preventing population explosion, which was observed to be especially common in the large dimensional instances, an $\epsilon$ value of 20 is selected for 2 and 3 dimensions as it produces the lowest population and the best spread. For the 4 dimensional problems an $\epsilon$ value of 50 is used, since the population size becomes quite large for these instances with $\epsilon = 20$. 

The end-of-run performance is calculated next, where the metrics are recorded after each run. Averages of metrics have been recorded along with standard deviation. Wilcoxon rank-sum test was performed as
a statistical measure to ascertain the level of significant difference between the algorithms. These values for the hypervolume metric are shown in Table \ref{hypervol}, for the spread metric are shown in Table \ref{spread} and for the generational distance in Table \ref{gendis}.


\begin{table}


\protect\caption{Spread measure values for NSGA-II and $\epsilon$-GC-AIS}\label{spread}
\centering
\def\arraystretch{1.2}
\begin{tabular}{|c|c|c|c|c|c|c|}
\hline 
\multicolumn{1}{|c}{} &  & \multicolumn{1}{c}{$\epsilon$-GC-AIS} &  & \multicolumn{1}{c}{NSGA-II} &  & \tabularnewline
\hline 
Sacks & items & average & std.dev & average & std.dev & Wilcoxon test\tabularnewline
\hline 
2 & 100 & \textbf{0.507} & 0.037 & 0.568 & 0.056 & 5.09e-6\tabularnewline
\hline 
 & 250 & 0.671 & 0.024 & 0.668 & 0.050 & 0.923\tabularnewline
\hline 
 & 500 & \textbf{0.666} & 0.027 & 0.710 & 0.040 & 5.26e-5\tabularnewline
\hline 
 & 750 & \textbf{0.493} & 0.044 & 0.645 & 0.031 & 3.01e-11\tabularnewline
\hline 
3 & 100 & \textbf{0.350} & 0.020 & 0.388 & 0.027 & 5.59e-7\tabularnewline
\hline 
 & 250 & \textbf{0.335} & 0.011 & 0.439 & 0.026 & 3.01e-11\tabularnewline
\hline 
 & 500 & \textbf{0.363} & 0.010 & 0.455 & 0.025 & 3.33e-11\tabularnewline
\hline 
 & 750 & \textbf{0.394} & 0.010 & 0.471 & 0.025 & 3.68e-11\tabularnewline
\hline 
4 & 100 & \textbf{0.298} & 0.018 & 0.361 & 0.020 & 5.49e-11\tabularnewline
\hline 
 & 250 & \textbf{0.317} & 0.011 & 0.406 & 0.025 & 3.33e-11\tabularnewline
\hline 
 & 500 & \textbf{0.304} & 0.007 & 0.440 & 0.020 & 3.01e-11\tabularnewline
\hline 
 & 750 & \textbf{0.323} & 0.006 & 0.454 & 0.021 & 3.02e-11\tabularnewline
\hline 
\end{tabular}

\end{table}
 
It can be seen from Table \ref{hypervol} that in six out of the twelve instances $\epsilon$-GC-AIS achieves greater hypervolume
than NSGA-II. While on three instances NSGA-II performs better than $\epsilon$-GC-AIS. In all but three instances
the Wilcoxon rank sum test was able to confirm significant difference between the two algorithms at a significance level of 0.05.
  
Values from the spread metric from Table \ref{spread} show that the non-dominated fronts achieved by $\epsilon$-GC-AIS
are better distributed than the ones achieved by NSGA-II. Except for one instance, the wilcoxon rank sum test confirmed significant difference between the two algorithms. Similar results can be seen from Table \ref{gendis} where for each of the instances it can be seen that the generational distance measure for the NSGA-II is higher than $\epsilon$-GC-AIS which means that the non-dominated fronts achieved by $\epsilon$-GC-AIS are closer to the reference fronts than those of NSGA-II. In this case the wilcoxon rank sum test showed significant difference between the two algorithms for
all the instances.

\begin{table}

\centering
\protect\caption{Generational distance measure for $\epsilon$-GC-AIS and NSGA-II}\label{gendis}
\def\arraystretch{1.2}
\begin{tabular}{|c|c|c|c|c|c|c|}
\hline 
 \multicolumn{1}{|c}{} &  & \multicolumn{1}{c}{$\epsilon$-GC-AIS} &  & \multicolumn{1}{c}{NSGA-II} &  & \tabularnewline

\hline 
Sack & items & average & std.dev & average & std.dev & Wilcoxon test\tabularnewline
\hline 
2 & 100 & \textbf{0.0010} & 3.93e-4 & 0.0038 & 6.37e-4 & 3.01e-11\tabularnewline
\hline 
 & 250 & \textbf{0.0015} & 1.75e-4 & 0.0071 & 6.86e-4 & 3.01e-11\tabularnewline
\hline 
 & 500 & \textbf{0.0029} & 2.56e-4 & 0.0137 & 8.62e-4 & 3.01e-11\tabularnewline
\hline 
 & 750 & \textbf{0.0013} & 2.70e-4 & 0.0166 & 0.0010 & 3.01e-11\tabularnewline
\hline 
3 & 100 & \textbf{0.0015} & 1.51e-4 & 0.0079 & 5.82e-4 & 3.01e-11\tabularnewline
\hline 
 & 250 & \textbf{9.67e-4} & 1.51e-4 & 0.0146 & 0.0013 & 3.01e-11\tabularnewline
\hline 
 & 500 & \textbf{8.43e-4} & 1.52e-4 & 0.0180 & 0.0011 & 3.01e-11\tabularnewline
\hline 
 & 750 & \textbf{8.41e-4} & 1.27e-4 & 0.0117 & 8.14e-4 & 3.01e-11\tabularnewline
\hline 
4 & 100 & \textbf{0.0028} & 2.73e-4 & 0.0142 & 8.89e-4 & 3.00e-11\tabularnewline
\hline 
 & 250 & \textbf{0.0011} & 1.46e-4 & 0.0199 & 0.0012 & 3.01e-11\tabularnewline
\hline 
 & 500 & \textbf{7.64e-4} & 8.35e-5 & 0.0182 & 0.0011 & 3.01e-11\tabularnewline
\hline 
 & 750 & \textbf{7.92e-4} & 1.57e-4 & 0.0144 & 7.18e-4 & 3.01e-11\tabularnewline
\hline 
\end{tabular}

\end{table} 

 
\section{Conclusion}

It can be seen from Figure \ref{plots} that setting the right $\epsilon$ value is important for maintaining the population size in the $\epsilon$-GC-AIS. Along with regulating the population size, it provides the added advantage of maintaining diversity between the solutions. Parameter setting is a crucial factor when employing any meta-heuristic to solve a problem. It can be seen from Table \ref{parameters} that $\epsilon$-GC-AIS requires the $\epsilon$ parameter as well as mutation rate to be set while NSGA-II requires population size, as well as mutation rate and crossover probability. 

We have shown that $\epsilon$-GC-AIS performs better than NSGA-II on the MOd-KP instances provided by \cite{zt1999a}. The value of $\epsilon$ has a big impact on the population size of $\epsilon$-GC-AIS, which can be seen from Figure \ref{plots}. In the higher dimensional cases
the number of non-dominated solutions increases very rapidly and possibly larger $\epsilon$ values could further improve hypervolume in those instances in Table \ref{hypervol}. $\epsilon$ is a new parameter which was not present in the original description of GC-AIS. We would like to remove the task of setting $\epsilon$ manually by incorporating some form of dynamic $\epsilon$ resizing, where this value will dynamically adjust according to population size and the number of fitness evaluations expended so far, and also do this for each dimension. This is a direction for future work. 

As future work we would also like to incorporate the correlated instances of MOd-KP as suggested in \cite{DBLP:journals/eor/ShahR11} and test the performance  on the many objective knapsack problem. Though NSGA-II is a popular multi-objective evolutionary algorithm and with incorporation of the weighted repair approach can be considered as one of the state of the art MOEAs, we would like to include other heuristics and MOEAs for future studies for MOd-KP.

 


\bibliographystyle{splncs03}
\bibliography{joshi}



\end{document}
